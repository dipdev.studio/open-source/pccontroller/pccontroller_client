import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PCContoller',
      theme: ThemeData(
        //primaryColor: Colors.black54,
        accentColor: Colors.orange,
        brightness: Brightness.dark,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isConnected = false;
  String errorText;
  RawDatagramSocket udpSocket;
  final int port = 7660;
  InternetAddress clientAddress;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PCController'),
        actions: isConnected
            ? [
                IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    setState(() {
                      udpSocket?.close();
                      udpSocket = null;
                      isConnected = false;
                      errorText = null;
                    });
                  },
                )
              ]
            : [],
      ),
      body: Container(
          padding: EdgeInsets.all(24.0),
          child:
              isConnected ? connectedWidget(context) : connectWidget(context)),
    );
  }

  Widget connectedWidget(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            icon: Icon(
              Icons.volume_up,
            ),
            iconSize: 64.0,
            onPressed: () => sendData('volume_up'),
          ),
          SizedBox(
            height: 24.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: Icon(
                  Icons.fast_rewind,
                ),
                iconSize: 64.0,
                onPressed: () => sendData('fast_rewind'),
              ),
              SizedBox(
                width: 24.0,
              ),
              IconButton(
                icon: Icon(
                  Icons.volume_mute,
                ),
                iconSize: 64.0,
                onPressed: () => sendData('volume_mute'),
              ),
              SizedBox(
                width: 24.0,
              ),
              IconButton(
                icon: Icon(
                  Icons.fast_forward,
                ),
                iconSize: 64.0,
                onPressed: () => sendData('fast_forward'),
              ),
            ],
          ),
          SizedBox(
            height: 24.0,
          ),
          IconButton(
            icon: Icon(
              Icons.volume_down,
            ),
            iconSize: 64.0,
            onPressed: () => sendData('volume_down'),
          ),
        ],
      ),
    );
  }

  Widget connectWidget(BuildContext context) {
    final ipController = TextEditingController();
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: ipController,
            decoration: InputDecoration(
              labelText: 'IP address',
              hintText: '0.0.0.0',
              icon: Icon(Icons.network_wifi),
            ),
            textAlign: TextAlign.center,
          ),
          if (errorText != null)
            SizedBox(
              height: 24.0,
            ),
          if (errorText != null)
            Text(
              errorText,
              style: Theme.of(context)
                  .textTheme
                  .bodyText2
                  .copyWith(color: Theme.of(context).errorColor),
            ),
          SizedBox(
            height: 24.0,
          ),
          ElevatedButton(
              onPressed: () async {
                if (ipController.text.isNotEmpty) {
                  try {
                    clientAddress = InternetAddress(ipController.text);
                  } catch (onError) {
                    setState(() {
                      isConnected = false;
                      udpSocket?.close();
                      udpSocket = null;
                      errorText = onError.toString();
                    });
                    return;
                  }

                  udpSocket = await connect().catchError((onError) {
                    setState(() {
                      isConnected = false;
                      udpSocket?.close();
                      udpSocket = null;
                      errorText = onError;
                    });
                  });
                  setState(() {
                    isConnected = true;
                    errorText = null;
                  });
                }
              },
              child: Text('Connect'))
        ],
      ),
    );
  }

  Future<RawDatagramSocket> connect() {
    return RawDatagramSocket.bind(InternetAddress.anyIPv4, port);
  }

  bool sendData(String input) {
    if (clientAddress == null) return false;
    final data = utf8.encode(input);
    return udpSocket.send(data, clientAddress, port) != 0;
  }

  @override
  void dispose() {
    udpSocket?.close();
    udpSocket = null;
    super.dispose();
  }
}
